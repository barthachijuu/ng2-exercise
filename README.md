# ng2-exercise

## __Set up__

Before cloning the repo **be sure** you have installed:

* [NodeJs & npm](http://nodejs.org/download/) (version >= 6)

- Choose a folder project in your system and switch in `cd [folder path]`
- Clone the repo in your folder `git clone https://github.com/barthachijuu/ng2-exercise.git`

## __Node Modules__
The standar node modules, are also installed globally on the folder.

## __Create New Project__
Start a new project with `ng new name_of_project --skip-install=true --directory=ng2-exercise` and the [Angular CLI](https://github.com/angular/angular-cli) create for you a new project. 

## __Development server__

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## __Code scaffolding__

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## __Build__

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## __Running unit tests__

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## __Running end-to-end tests__

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## __Further help__

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## __Contributing__

- Fork it!
- Create your feature branch: `git checkout -b my-new-feature`
- Commit your changes: `git commit -am 'Add some feature'`
- Push to the branch: `git push origin my-new-feature`

---


## __Credits__

- [Bartolo Amico](https://github.com/barthachijuu) (repository crator)
- [Barnaba Longo](https://github.com/balofg/) (scaffolding creation)

---